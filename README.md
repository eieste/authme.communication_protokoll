AuthMe.Protokoll Definition
=========

Dies ist die Dokumentation für den Datenverkehr zwischen
AuthMe.de und einem anderen Server.
Diese Kommunikation gilt für die folgenden AuthMethoden
 - PingBack

Es gibt zwei JSONs die ineinander Verschachtelt sind!
 - Communication json
 - crypted-data json

Das Communication json, ist wie ein "header", der mit gesendet wird, um die Daten die in dem crypted-data json stehen zu Verifizieren.

Aktuelle mögliche Protokolle
-----------
 * [Communication json](https://bitbucket.org/eieste/authme.communication_protokoll/src/master/authme.com/)'s (Hier gibts nur eins ;) )
 * Crypted Data (Infos und Hilfen zum Verschlüsseln)
    * [AuthMe.de Login (PingBack Methode)](https://bitbucket.org/eieste/authme.communication_protokoll/src/master/authme.login:pingback/README.md)
    * [AuthMe.de Heartbeat](https://bitbucket.org/eieste/authme.communication_protokoll/src/master/authme.heartbeat/README.md)
    * [AuthMe.de.Direct Communication](https://bitbucket.org/eieste/authme.communication_protokoll/src/master/authme.direct_com/README.md)
         * [AuthMe.de Permission Check](https://bitbucket.org/eieste/authme.communication_protokoll/src/master/authme.direct_com/authme.direct_com:permission-check/README.md)

Links
-----------


* [AuthMe](http://authme.de)
* [AuthMe.Wiki](http://wiki.authme.de)