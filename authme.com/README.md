AuthMe.Protokoll Definition
=========

[Zurück](https://bitbucket.org/eieste/authme.communication_protokoll/src/master/README.md)


***
AuthMe.Protokoll:Communikation
---------
***

Files
-----------
 - [AuthMe Communikation JSON](https://bitbucket.org/eieste/authme.communication_protokoll/src/master/authme.com/authme.com.json)    ("Das, das so ähnlich wie ein Header ist ;D ")



Erklärung
-----------------------

Hier werden die Einzelnen Parameter des JSONS erklärt

version
---
```
...
    "version":0.1,
...
``` 
** Deutsch **

Dieser Parameter gibt die Version des Communication json Scripts an. Wenn dieser Parameter ist in allen Zukünftigen
Version vorhanden. Wenn Sie eine nachricht bekommen, mit einer höheren version, als die version für die Sie das Script entwickelt haben, kann
es sein, das ihr Script die Nachricht nicht versteht.

**Englisch**

It meanse the Version of this JSON-Script. The Version Parameter is in all Futured Versions included.
If yo develop a Script for Version 0.1 and authMe send you a script with a (futured) version 0.4, it can be your script
doesnt understand this version



key_version
---

```
...
    "key_version":1.0,
...
```
** Deutsch **

Wenn mein Server Public Key Korumpiert ist, werde ich einen Neuen erstellen. Geschiet dies, wird die key_version um eine Version
Erhöht. Wenn Sie den Server Key Heruntergeladen haben (nicht empfohlen! [sehen Sie hier](https://docuwiki.authme.de/doku.php?id=authme_api:crypt_server_key_api) was empfohlen ist), so sollten sie sich den Aktuellen Server Key herunterladen

** Englisch **

If the Server Public Key is Corrupted, then I generate a new one.
If that happens, the key_version update to a new Version. If you had download the Server key (not recommended, [see here](https://docuwiki.authme.de/doku.php?id=authme_api:crypt_server_key_api) what is recommended ), you should be redownload the key


timestamp
---

```
...
    "timestamp":1407854853,
...
``` 

** Deutsch **

Der wert des Parameters "timestamp" ist die UNIX UTC Zeit. Wenn Sie eine Inkommende Nachricht haben, sollten sie darauf achten, das der timestamp nicht in der Zukunft und nicht älter als 5 Minuten ist. Wenn dies der jedoch der fall ist, sollten sie die Nachricht Ignorieren 

** Englisch **

The Value of timestamp ist the UNIX UTC time! When you had a Incomming Message, with a Timestamp they are older than 5Minutes, so do you ignore the Incoming Transmission.


from
---

```
...
    "from":"AuthMe",
...
``` 

** Deutsch **

Wenn Sie eine Nachricht an AuthMe Schicken, sollte dieser wert den Namen ihrer Application enthalten, den sie im AuthMe. Developer Center definiert haben.
Wenn sie eine Nachricht von AuthMe.de Erhalten, enthält dieser Parameter den wert "AuthMe"


** Englisch **

You send a Message to AuthMe, so should the Value of from your Application name, that you are insert in the AuthMe.Developer Center

to
---

```
...
    "to":"AuthMe",
...
``` 

** Deutsch **

Wenn Sie eine Nachricht an AuthMe Schicken, sollte dieser wert "AuthMe" enthalten,
Erhalten Sie eine Nachricht von AuthMe, enthält dieser Parameter den Namen ihrer Application den sie auf AuthMe.de Definiert haben.

** Englisch **

On a Incomming Transmission from AuthMe to "Your Projekt", contains this parameter the name of "Your Projekt" that you defined on AuthMe.Developer Center.
When you Send a Message to AuthMe should contain this Parameter "AuthMe"

signature & entry
---

```
...
    "signature":"",
    "entry";""
...
``` 

** Deutsch **
 - [Lesen Sie hierzu diese Dokumentation Empfangen von AuthMe Nachrichten](https://docuwiki.authme.de/doku.php?id=authme_api:crypt_recive_message_from_authme)
 - Lesen Sie hierzu diese Dokumentation Versenden von Nachrichten zu AuthMe

** Englisch **

Read the following Documentation:

 - [Recive Message from AuthMe](https://docuwiki.authme.de/doku.php?id=authme_api:crypt_recive_message_from_authme)
 - Transmit Messages to AuthMe

